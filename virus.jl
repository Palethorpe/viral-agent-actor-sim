using Actors
import Actors: hear

"Indicate a new epoch has begun at time `t`"
struct Tick!{S}
    sim::S
    t::Int
    "The parent and return address"
    re::Id
end

"Signal the end of an epoch"
struct Tock!
    tick::Tick!
end

struct EpochBuffer
    odd::Vector
    even::Vector
end

struct OnTick!
    t::Int
    msg
    re::Id
end

struct AckOnTick! end

function hear(s::Scene, msg::OnTick!)
    push!(iseven(msg.t) ? my(s).buf.even : my(s).buf.odd, msg.msg)
    say(s, msg.re, AckOnTick!())
end

say_in(s::Scene, t::Tick!, to::Id, msg) = ask(s, to, OnTick!(t.t, msg, me(s)), AckOnTick!)

hear_epoch(s::Scene, t::Int) = let buf = my(s).buf
    epoch_msgs = isodd(t) ? buf.even : buf.odd

    foreach(m -> hear(s, m), epoch_msgs)

    empty!(epoch_msgs)
end

"""## Agent

A person or animal who may host the virus. There are many agents, each with
independant parameters shown below.

- _h_ The current health
- _H_ The baselilne health
- _v_ The viral load
- _i_ The immunity to the virus
- _a_ Virus Antibodies

"""
mutable struct Agent
    h::Float64
    H::Float64
    v::Float64
    i::Float64
    a::Float64
end

mutable struct AgentFrames
    frames::Vector{Agent}
    buf::EpochBuffer
end

AgentFrames(frames) = AgentFrames(frames, EpochBuffer([], []))

frame(s::Scene) = my(s).frames[end]

"""## World

A container for global parameters which effect all agents and Cells.

- _R_ Recovery rate; how fast the agent recovers health
- _D_ Damage rate; effectiveness of the immune response
- _C_ Contagion; how fast the virus replicates within an agent regardless of health
- _CH_ Contagion health bias; how fast the virus replicates proportional to health
- _A_ Antigen; Coefficient of antiviral level
- _I_ Immunity; how fast immunity is developed
- _S_ Shedding; rate of viral shedding into the cell

"""
struct World
    R::Float64
    D::Float64
    C::Float64
    CH::Float64
    A::Float64
    I::Float64
    S::Float64
end

Base.show(io::IO, w::World) = println(io, """World {
    R = $(w.R),
    D = $(w.D),
    C = $(w.C),
    CH = $(w.CH),
    A = $(w.A),
    I = $(w.I),
    S = $(w.S)
}""")

struct Dump!
    re::Id
end

function hear(s::Scene{AgentFrames}, msg::Dump!)
    n = length(my(s).frames)
    @assert n == 100 "$(me(s)) has $n frames"
    say(s, msg.re, my(s).frames)
end

"""## Cell

A location cosy enough for agents to share the virus

- `agents` The agents in this cell
- _v_ The virus shedded into this cell
"""
mutable struct Cell
    agents::Set{Id}
    v::Float64
end

Cell(agents::Vector, v) = Cell(Set(agents), v)

mutable struct CellFrames
    frames::Vector{Cell}
    buf::EpochBuffer
    c::Int
    parent::Id
end

CellFrames(frames) = CellFrames(frames, EpochBuffer([], []), 0, Id(1))

function hear(s::Scene{CellFrames}, msg::Tick!)
    my(s).parent = msg.re

    hear_epoch(s, msg.t)

    cell = frame(s)
    my(s).c = length(cell.agents)

    if isempty(cell.agents)
        say(s, me(s), Tock!(msg))
    else
        foreach(a -> say(s, a, Tick!(msg.sim, msg.t, me(s))), cell.agents)
    end
end

function hear(s::Scene{CellFrames}, k::Tock!)
    my(s).c -= 1

    if my(s).c < 1
        push!(my(s).frames, tick(s, k.tick, frame(s)))

        say(s, my(s).parent, Tock!(k.tick))
    end
end

function hear(s::Scene{CellFrames}, msg::Dump!)
    cell = frame(s)
    foreach(a -> say(s, a, Dump!(me(s))), cell.agents)

    say(s, msg.re, [expect(s, Vector{Agent}) for _ in cell.agents])
end

mutable struct WorldPlay
    sim
    t::Int
    c::Int
    cells::Vector{Id}
    observer::Id
end

function hear(s::Scene{WorldPlay}, ::Genesis!)
end

function hear(s::Scene{WorldPlay}, msg::Tock!)
    wp = my(s)
    wp.c -= 1

    @assert msg.tick.t == wp.t "Tock($(msg.tick.t)) does not match current tick($(wp.t)); c=$(wp.c)"

    if wp.c < 1
        wp.t -= 1

        if wp.t <= 1
            shout(s, wp.cells, Dump!(me(s)))
            say(s, wp.observer, vcat((expect(s, Vector) for _ in wp.cells)...))
            return
        end

        wp.c = length(wp.cells)

        shout(s, wp.cells, Tick!(wp.sim, wp.t, me(s)))        
    end
end

function hear(s::Scene{AgentFrames}, msg::Tick!)
    hear_epoch(s, msg.t)

    push!(my(s).frames, tick(s, msg, frame(s)))

    say(s, msg.re, Tock!(msg))
end

function plot_agents(res)
    fig = gcf(Figure((950, 600)))
    subplot(2, 2, 1)
    plot(map(a -> a.h, res); xlim=(1, 100), ylim=(0, 1))
    xlabel("Time t")
    ylabel("Baseline health H")
    subplot(2, 2, 2)
    plot(map(a -> a.v, res); xlim=(1, 100))
    xlabel("Time t")
    ylabel("Viral Load v")
    subplot(2, 2, 3)
    plot(map(a -> a.a, res); xlim=(1, 100))
    xlabel("Time t")
    ylabel("Antiviral a")
    subplot(2, 2, 4)
    plot(map(a -> a.i, res); xlim=(1, 100), ylim=(0, 1))
    xlabel("Time t")
    ylabel("Immunity i")
end
