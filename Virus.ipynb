{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "outputs": [],
   "source": [
    "using GRUtils"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Viral agent simulation\n",
    "\n",
    "Experiments with viral dynamics using an agent based simulation. Each agent is contained within an [actor](https://palethorpe.gitlab.io/Actors.jl/). There are two types of actor, a cell, which contains agents, and agents themselves. Cells are vertices in a graph and the connections, if any, between cells are the edges.\n",
    "\n",
    "An agent may be thought of as a person or animal and a cell a physical location intimate enough that if two or more agents share a cell, they will also share the virus. Initially there are two hypotheses I would like to test with this, one is that _if_ significant viral load can be shared between agents due to shedding, then a seeminly mild virus can suddenly wipe out a population (critical mass). Secondly that limited sampling/testing can lead to surprising jumps in reported spread. Of course this is not a remotely realistic model, it is just supposed to show some general effects observable in any complex system which resembles viral spread.\n",
    "\n",
    "Some of the implementation is in the following external Juila script. Some structures defined there are shown below. I'm not a biologist so don't expect the names of parameters to be fully inline with the literature (putting it mildly).\n",
    "\n",
    "**If you are getting bored, just scroll down until you see graphs**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true,
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "include(\"virus.jl\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "@doc Agent"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "@doc Cell"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "@doc World"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "W = World(1.025, 0.25, 1.11, 0.11, 0.9, 1.6, 0.05)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Single cell sim without shedding\n",
    "\n",
    "For each tick _t_ of the world clock the agents are updated as follows. Note that the below function takes the current agent structure and returns a new one with updated values. This gets called by the agent actor in `virus.jl`. I'm sure the system of equations this represents lends itself to various mathematical analyses, but will ignore that for now and just burn some CPU cycles instead. I jiggled the parameters around until I got something that I vaguely wanted."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "∇(w::World, a::Agent) = \n",
    "    Agent(min(w.R*a.h, a.H) - a.v - a.a, \n",
    "          a.H,\n",
    "          max(a.v*(w.C + w.CH*a.h) - w.D*a.a - w.S*a.v, 0.0),\n",
    "          max(a.i + w.I*a.v, 0),\n",
    "          max(w.A*(a.a + a.v*a.i*a.h), 0))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Some observations; the agent's health is damaged both by the virus and the 'antivirals'. With some viruses it is theorised that the immune response is what kills the patient in most cases, not the virus. The increase in virus can be accellerated by the agent's health, a healthy agent may have more resources for the virus or selective pressure may make the virus less aggressive when the host is already sick. To simplify things all agents develop immunity at the same rate, but healthier agent's can produce 'antivirals' quicker. Infact a healthy agent is more likely to over produce antivirals.\n",
    "\n",
    "Frankly we could slap any number of equations together and throw some interpretation on it. It is important to understand that I'm not trying to forecast for a particular virus, instead I am just trying to observe some emergent behaviour."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "\"A message containing the starting parameters for a Single celled simulation\"\n",
    "struct SingleCellSim!\n",
    "    W::World\n",
    "    seed::Float64\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "For each tick we have some callbacks for agents and cells. The callback returns a new Agent or Cell for time `t`. It is called by the message handler for `Tick!` in `virus.jl`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "tick(::Scene, tick::Tick!{SingleCellSim!}, a::Agent) = ∇(tick.sim.W, a)\n",
    "tick(::Scene, ::Tick!{SingleCellSim!}, c::Cell) = c"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "Below is a message handler for an actor which creates the simulation and returns the results. In this case it just creates some agents with different baseline health values and runs a sim for 100 ticks. All the agents are put in the same cell, but they don't interact with each other because the shedding is ignored in the update rule above (although it is deducted from the viral load).\n",
    "\n",
    "Each agent starts with a small amount of viral load and no immunity.\n",
    "\n",
    "If you interested in how this works then `invite!` adds actors to the system and `say/ask` sends messages to actors. The actual actors are `CellFrames` and `AgentFrames` which store the `Cell` and `Agent` state at each tick."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "function hear(s::Scene{WorldPlay}, sim::SingleCellSim!)\n",
    "    wp = my(s)\n",
    "    agents = [invite!(s, AgentFrames([Agent(h, h, sim.seed, 0, 0)])) for h in 0.1:0.1:1]\n",
    "    wp.cells = [invite!(s, CellFrames([Cell(agents, 0)]))]\n",
    "    wp.sim = sim\n",
    "    wp.t = 100\n",
    "    \n",
    "    # Send the first tick, the Cell will tick the agents recursively and they will return tock!\n",
    "    # The tocks are handled by a message handler in virus.jl which then sends out the next tick\n",
    "    shout(s, wp.cells, Tick!(wp.sim, wp.t, me(s)))\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "Start the actor system..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true,
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "@isdefined(s) && Actors.try_say(s, stage(s), Leave!())\n",
    "s, t = interact!(WorldPlay(nothing, 0, 0, [], Id(1)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "Ask the `WorldPlay` actor (via the `Stage` actor) to run the simulation by sending it a message containing the `World` parameters (amongst other things particular to the Actors.jl library). What we get back is an array of agent states for each tick. This is mapped into a matricies of health and virual load values and displayed. Note that the order of the columns in the matrix is semi-random because the agent data is collected asynchronously."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "res = ask(s, stage(s), SingleCellSim!(W, 0.001), Vector);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false,
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "res = vcat(res...) |> x -> reshape(x, 100, :)\n",
    "sort!(res, dims=2; by=a -> a.H);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "Plot the agent properties throughtout the simulation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false,
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "plot_agents(res)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "So the agents with a lower baseline health are killed off while the higher ones are barely effected. Agents with moderate health take much longer to recover and take longer to develop immunity. Lets try running it again, but with a much lower seed value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false,
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "res = vcat(ask(s, stage(s), SingleCellSim!(W, 0.0001), Vector)...) |> x -> reshape(x, 100, :)\n",
    "sort!(res, dims=2; by=a -> a.H);\n",
    "plot_agents(res)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "This delays the onset of symptoms (visible dip in health) and reduces the amplitude of lost health. You may note that viral load goes wild for the two agents who reach a health of zero (as does immunity, but that can be ignored). I suppose this could make sense as the immune system is totally defeated. The viral load then begins to drop as the virus runs out of cells to infect and decomposition begins creating a hostile environment for the virus."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Single cell sim with shedding\n",
    "\n",
    "Now lets try simulating spreading via shedding. I would think that in real life the amount of viral load gainded through someone elses shedded viral particulates would be too small to significantly increase your own load. Unless sharing bodily fluids in significant amounts ofcourse. However this could also model being exposed to different strains or some unknown mechanism which increases viral load or inflamation when individuals are clustered together.\n",
    "\n",
    "Actually, as it happens, there are some papers floating about which suggest that the initial dose of virus through aerosols can have a significant effect on severity, so this is maybe not so far fetched."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "struct SingleCellSheddingSim!\n",
    "    W::World\n",
    "    seed::Float64\n",
    "end\n",
    "\n",
    "struct Shed!\n",
    "    v::Float64\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Below we create some message handlers for the `Shed!` message above. This is a 'timed' message which is used to transfer virus from infected agents to the environment to other agents. The message is transmitted in such a way that it is guaranteed to be delivered within a given epoch (that is at time `t`) so that there aren't any data races."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "function hear(s::Scene, msg::Shed!)\n",
    "    frame(s).v += msg.v\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A new `tick` callback handlers are defined which sends the `Shed!` message with `say_in` which guarantees the message will be processed within a single tick. Usually messages are not guaranteed to be delivered in particular order or during a given time frame, so some extra work needs to be done to ensure that."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "function tick(s::Scene, tick::Tick!{SingleCellSheddingSim!}, a::Agent)\n",
    "    say_in(s, tick, tick.re, Shed!(a.v*tick.sim.W.S))\n",
    "    \n",
    "    ∇(tick.sim.W, a)\n",
    "end\n",
    "\n",
    "function tick(s::Scene, tick::Tick!, c::Cell)\n",
    "    foreach(a -> say_in(s, tick, a, Shed!(c.v / 10)), c.agents)\n",
    "    \n",
    "    Cell(copy(c.agents), 0)\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For now we assume the maximum number of agents which can occupy a single cell is 10 and the proportion of transmitted virus will increase linearly as the number of agents occupying the cell increases. It is unlikely to be linear in a real 3D environment, but it may serve as a reasonable approximation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "function hear(s::Scene{WorldPlay}, sim::SingleCellSheddingSim!)\n",
    "    wp = my(s)\n",
    "    agents = [invite!(s, AgentFrames([Agent(h, h, 0, 0, 0)])) for h in 0.2:0.1:1]\n",
    "    push!(agents, invite!(s, AgentFrames([Agent(0.5, 0.5, sim.seed, 0, 0)])))\n",
    "    \n",
    "    wp.cells = [invite!(s, CellFrames([Cell(agents, 0)]))]\n",
    "    wp.sim = sim\n",
    "    wp.t = 100\n",
    "    \n",
    "    cell = invite!(s, CellFrames([Cell(agents, 0)]))\n",
    "    \n",
    "    shout(s, wp.cells, Tick!(wp.sim, wp.t, me(s)))\n",
    "end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "@isdefined(s) && Actors.try_say(s, stage(s), Leave!())\n",
    "s, t = interact!(WorldPlay(nothing, 0, 0, [], Id(1)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "res = vcat(ask(s, stage(s), SingleCellSheddingSim!(W, 0.001), Vector)...) |> x -> reshape(x, 100, :)\n",
    "sort!(res, dims=2; by=a -> a.H);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_agents(res)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This makes things a lot worse, even the most healthy agent takes a significant dip. I suppose that is to be expected though when there are some corpses lying in the room. You may notice there is a line which begins dipping before the others and sort of waves a bit; that is the super spreader.\n",
    "\n",
    "There is no mass extinction here, but ofcourse we could fiddle with the params until there is."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Multi-cell sim\n",
    "\n",
    "Things can get vastly more complex moving to a mult-cell sim. We have to decide on the topology of the cells; whether they are all connected or what type of graph they form and so on. We also have to decide on agent behaviour, as they are now able to move around.\n",
    "\n",
    "To keep things simple we will start with a fully connected graph; all cells are connected and agents can move freely between them. Imagine a spacious village with equally sized huts arranged in a circle. Each cell is a hut and the villagers teleport between them...\n",
    "\n",
    "Also to keep things simple, agent behaviour will be random. For each tick there is some possibility that an agent will try to move to another hut picked at random, health permitting."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "struct VillageSim!\n",
    "    W::World\n",
    "    \"Probability any given agent will move\"\n",
    "    P::Float64\n",
    "    \"Nested array which represents cells with agents in\"\n",
    "    cells::Vector{Vector{Agent}}\n",
    "end\n",
    "\n",
    "struct VillageSim\n",
    "    W::World\n",
    "    P::Float64\n",
    "    cells::Vector{Id}\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Above you can see we now specify the cells in the simulation message as a nested vector; moving towards a declarative specification for the simulation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "function hear(s::Scene{WorldPlay}, simdef::VillageSim!)\n",
    "    wp = my(s)\n",
    "    \n",
    "    \n",
    "    wp.cells = [invite!(s, CellFrames([Cell([invite!(s, AgentFrames([a])) for a in agents], 0)]))\n",
    "                    for agents in simdef.cells]\n",
    "    wp.c = length(wp.cells)\n",
    "    wp.sim = VillageSim(simdef.W, simdef.P, wp.cells)\n",
    "    wp.t = 100\n",
    "    \n",
    "    shout(s, wp.cells, Tick!(wp.sim, wp.t, me(s)))\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When an agent decides to move it sends a couple of timed messages. One to the cell it wishes to move to and one to the cell it is moving from."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "struct Ingress!\n",
    "    who::Id\n",
    "end\n",
    "\n",
    "struct Egress!\n",
    "    who::Id\n",
    "end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "hear(s::Scene{CellFrames}, msg::Ingress!) = push!(frame(s).agents, msg.who)\n",
    "hear(s::Scene{CellFrames}, msg::Egress!) = delete!(frame(s).agents, msg.who)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The agent decides to move in the `tick` callback."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "function tick(s::Scene, tick::Tick!{VillageSim}, a::Agent)\n",
    "    say_in(s, tick, tick.re, Shed!(a.v*tick.sim.W.S))\n",
    "    \n",
    "    if rand() > tick.sim.P && a.h > 0.3\n",
    "        cell = rand(tick.sim.cells)\n",
    "        \n",
    "        if cell ≠ tick.re\n",
    "            say_in(s, tick, cell, Ingress!(me(s)))\n",
    "            say_in(s, tick, tick.re, Egress!(me(s)))\n",
    "        end\n",
    "    end\n",
    "    \n",
    "    ∇(tick.sim.W, a)\n",
    "end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "@isdefined(s) && Actors.try_say(s, stage(s), Leave!())\n",
    "s, t = interact!(WorldPlay(nothing, 0, 0, [], Id(1)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "sim = VillageSim!(W, 0.9, [\n",
    "        [Agent(1, 1, 0.001, 0, 0), Agent(1, 1, 0, 0, 0)],\n",
    "        [[Agent(h, h, 0, 0, 0), Agent(h + 0.1, h + 0.1, 0, 0, 0)] for h in 0.1:0.2:0.8]...])\n",
    "\n",
    "res = vcat(ask(s, stage(s), sim, Vector)...) |> x -> reshape(x, 100, :)\n",
    "sort!(res, dims=2; by=a -> a.H);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_agents(res)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you might expect this producers a more random set of curves as agents move around infecting each other."
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Julia 1.4.0",
   "language": "julia",
   "name": "julia-1.4"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.4.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
