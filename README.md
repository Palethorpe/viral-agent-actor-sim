Because everyone should have a virus simulation. This one is an agent based
simulation which I have implemented using the actor model (Actors.jl).

The pre-generated output is here:
https://palethorpe.gitlab.io/viral-agent-actor-sim/

To run it yourself you need Julia 1.3+ and Jupyter. Also the Juila packages
IJulia, Actors and GRUtils. You should set `JULIA_NUM_THREADS=$(nproc)` to
take advantage of multithreading.
